section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .increase:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .increase
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    mov r9, rsp
    dec rsp
    mov byte [rsp], 0
    .loop:
        mov rdx, 0
        div rsi
        add dl, 0x30
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, r9
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .end
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .end:
        jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .no
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .no:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    mov rsi, rsp
    mov rax, 0
    mov rdi, 0
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r8, 0
    .skip:
        push rdi
        push rsi
        push r8
        call read_char
        pop r8
        pop rsi
        pop rdi
        cmp al, 0x20
        je .skip
        cmp al, 0x9
        je .skip
        cmp al, 0xA
        je .skip
        test al, al
        jz .end
    .read_data:
        mov byte [rdi+r8], al
        inc r8
        push rdi
        push rsi
        push r8
        call read_char
        pop r8
        pop rsi
        pop rdi
        dec rsi
        cmp r8, rsi
        jg .error
        inc rsi
        cmp al, 0x20
        je .end
        cmp al, 0x9
        je .end
        cmp al, 0xA
        je .end
        test al, al
        jz .end
        jmp .read_data
    .error:
        mov rax, 0
        ret
    .end:
        mov byte [rdi+r8], 0
        mov rax, rdi
        mov rdx, r8
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov rcx, 0
    mov rsi, 10
    .compare:
        mov r8, 0
        mov r8b, byte [rdi+rcx]
        cmp r8b, '9'
        ja .end
        cmp r8b, '0'
        jb .end
        inc rcx
        mul rsi
        sub r8, 0x30
        add rax, r8
        jmp .compare
    .end:
        mov rdx, rcx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
    cmp al, '-'
    je .sign
    jmp parse_uint
    .sign:
        inc rdi
        call parse_uint
        neg rax
        test rdx, rdx
        jz .end
        inc rdx
    .end:
        ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .error
    .copy:
        mov dl, byte [rdi]
        mov byte [rsi], dl
        inc rdi
        inc rsi
        test dl, dl
        jnz .copy
        ret
    .error:
        mov rax, 0
        ret
